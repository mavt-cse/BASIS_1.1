% Two examples for using BASIS
% Both examples are using uniform prior

% Main flow of usage: 
%   (1) Define a likelihood function (format instructed in User_Input)
%   (2) Setup User_Input() and store the output of system parameters
%   (3) Feed the output into BASIS_Master to run BASIS
% Note: details of functionalities of BASIS are in User_Input

example_index = 2; % 1 - 3D Gaussian likelihood, 2 - Banana likelihood
Ns = 1000; % number of samples

%% Running algorithm

switch example_index
    case 1
        % for User_Input()
        in_para.Ns = Ns;
        in_para.Nd = 3;
        in_para.lik_name = 'Lik_Gauss_3D';
        in_para.lik_para.mu = [0 0 0];
        in_para.lik_para.sig = [1 0.5 0;
               0.5 1 -0.8;
               0 -0.8 1];
        in_para.bounds = [-5 -5 -5 ; 5 5 5];
        % for plotting
        ind_dim = [1 2; 2 3; 1 3]; % pairs of plotted dim [#figure x 2]
    case 2
        % for User_Input()
        in_para.Ns = Ns;
        in_para.Nd = 2;
        in_para.lik_name = 'Lik_Banana';
        in_para.lik_para = [];
        in_para.bounds = [-10 -10 ; 10 10];
        % for plotting
        ind_dim = [1 2]; % pairs of plotted dim [#figure x 2]
end

% The default User_Input() has no input argument, but users can add one
% themselves as in this example
sys_para = User_Input_Example(in_para);

BASIS = BASIS_Master(sys_para);

% delete(gcp)
% exit

%% Plot results (copied from Plot_Results.m)

% Assuming dimension > 1 for theta
f_seed = 1; % index of first figure
size_t = 20; % title font size
size_xy = 16; % x,y label font size
size_line = 2; % line width
size_mark = 10; % marker size
size_dot = 50; % scatter dot base size
load('BASIS_output.mat')

t_pause = 5; % sec of pause when plotting intermediate samples


% Plotting final results
figure(f_seed)
tmp = out_master.N_accept(2:end)./(out_master.N_accept(2:end) + out_master.N_reject(2:end));
plot(2:out_master.gen,tmp,'-xk','LineWidth',size_line,'MarkerSize',size_mark)
title('BASIS statistics','FontSize',size_t)
xlabel('Stage','FontSize',size_xy)
ylabel('Acceptance rate','FontSize',size_xy)
xlim([1 out_master.gen])
ylim([0 1])

for i = 1:size(ind_dim,1)
    figure(f_seed+i)
    clf
    tmp = out_master.lik + out_master.pri;
    scatter(out_master.theta(:,ind_dim(i,1)),out_master.theta(:,ind_dim(i,2)),...
        size_dot*(1+0.1*out_master.Ns),tmp)
    title('Final samples','FontSize',size_t)
    xlabel(['Dim. ',num2str(ind_dim(i,1))],'FontSize',size_xy)
    ylabel(['Dim. ',num2str(ind_dim(i,2))],'FontSize',size_xy)
    
    c = colorbar;
    c1=get(gca,'position');
    c2=get(c,'Position');
    c2(3)=0.5*c2(3);
    set(c,'Position',c2)
    set(gca,'position',c1)
end

% Plotting intermediate samples
for k = 1:out_master.gen-1
    load(['BASIS_gen_',num2str(k),'.mat'])
    for i = 1:size(ind_dim,1)
        figure(f_seed+i)
        clf
        tmp = runinfo.lik + runinfo.pri;
        scatter(runinfo.theta(:,ind_dim(i,1)),runinfo.theta(:,ind_dim(i,2)),...
            size_dot*(1+0.1*runinfo.Ns),tmp)
        title(['Stage ',num2str(k)],'FontSize',size_t)
        xlabel(['Dim. ',num2str(ind_dim(i,1))],'FontSize',size_xy)
        ylabel(['Dim. ',num2str(ind_dim(i,2))],'FontSize',size_xy)
        
        c = colorbar;
        c1=get(gca,'position');
        c2=get(c,'Position');
        c2(3)=0.5*c2(3);
        set(c,'Position',c2)
        set(gca,'position',c1)
    end
    pause(t_pause)
end
load('BASIS_output.mat')
for i = 1:size(ind_dim,1)
    figure(f_seed+i)
    clf
    tmp = out_master.lik + out_master.pri;
    scatter(out_master.theta(:,ind_dim(i,1)),out_master.theta(:,ind_dim(i,2)),...
        size_dot*(1+0.1*out_master.Ns),tmp)
    title('Final samples','FontSize',size_t)
    xlabel(['Dim. ',num2str(ind_dim(i,1))],'FontSize',size_xy)
    ylabel(['Dim. ',num2str(ind_dim(i,2))],'FontSize',size_xy)
    
    c = colorbar;
    c1=get(gca,'position');
    c2=get(c,'Position');
    c2(3)=0.5*c2(3);
    set(c,'Position',c2)
    set(gca,'position',c1)
end