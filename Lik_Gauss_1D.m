function [out,misc] = Lik_Gauss_1D(theta,para)

out = log(normpdf(theta));

misc.abc = para;
misc.test = out;

end