function [out,misc] = Lik_Gauss_3D(theta,para)

out = log(mvnpdf(theta,para.mu,para.sig));

misc.test = out;

end